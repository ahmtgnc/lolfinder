package huseyinyilmaz.lolfinder.intro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import java.util.Locale;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.user_operations.MainPageActivity;


public class MainActivity extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String language = sharedpreferences.getString("language", "");
        setLocale(language);


        StrictMode.setThreadPolicy(new StrictMode.
                ThreadPolicy.Builder().
                detectDiskReads().
                detectDiskWrites().
                detectNetwork().
                penaltyLog().build());
        Thread welcomeThread = new Thread() {

            @Override
            public void run() {
                try {
                    super.run();

                    sleep(3500);  //Delay of 5 seconds
                } catch (Exception e) {

                } finally {

                    Intent i = new Intent(MainActivity.this, MainPageActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        welcomeThread.start();
    }

    private void setLocale(String language) {
        Locale myLocal = new Locale(language);
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(myLocal);
        resources.updateConfiguration(configuration, dm);
    }
}
