package huseyinyilmaz.lolfinder.rest;

import android.app.Activity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ahmetgenc on 25/06/2017.
 */

public class HttpConnection extends Activity {

    public HttpURLConnection httpURLConnection;

    public HttpURLConnection connectToServerPost(URL url, JSONObject json) throws IOException, JSONException {
        httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
        httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8"); // here you are setting the `Content-Type` for the data you are sending which is `application/json`
        httpURLConnection.setRequestProperty("Accept", "application/json");
        httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");

        System.out.println(json.toString() + " giden json objesi");
        OutputStreamWriter osw = new OutputStreamWriter(httpURLConnection.getOutputStream());
        osw.write(json.toString());
        osw.flush();
        osw.close();
        httpURLConnection.connect();
        int status = httpURLConnection.getResponseCode();
        if (status >= 390)
            System.out.println(httpURLConnection.getErrorStream() + " stream*************************************************************************************************");
        else
            System.out.println(status + " stream response code*************************************************************************************************");
        return httpURLConnection;
    }

    public void disconnectFromServer() throws IOException {
        httpURLConnection.disconnect();
    }


}
