package huseyinyilmaz.lolfinder.user_operations;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.rest.HttpConnection;
import huseyinyilmaz.lolfinder.util.Utility;

public class ForgotPassEnterCodePopActivity extends AppCompatActivity {

    EditText enterCodeToResetPass;
    EditText enterNewPass;
    EditText enterNewPassRepeat;
    TextClock textClock;
    Button resetPasswordBtn;

    HttpConnection connection;

    String userMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_enter_mail_pop);
        setPopWindow();

        textClock = (TextClock) findViewById(R.id.enterCodeClock);
        enterCodeToResetPass = (EditText) findViewById(R.id.forgotPassEnterCode);
        enterNewPass = (EditText) findViewById(R.id.forgotPassNewPassEdt);
        enterNewPassRepeat = (EditText) findViewById(R.id.forgotPassNewPassReEdt);
        resetPasswordBtn = (Button) findViewById(R.id.forgotPassResetPassBtn2);

        userMail = getIntent().getStringExtra("mail");

        new CountDownTimer(10 * 60000, 1000) {

            public void onTick(long millisUntilFinished) {
                textClock.setText(new SimpleDateFormat("mm:ss").format(new Date(millisUntilFinished)));
            }

            public void onFinish() {
                Toast.makeText(ForgotPassEnterCodePopActivity.this, getApplicationContext().getString(R.string.codeAgain), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ForgotPassEnterCodePopActivity.this, ForgotPasswordPopActivity.class);
                startActivity(i);
            }
        }.start();

        resetPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enterNewPass.getText().toString().
                        equals(enterNewPassRepeat.getText().toString())) {
                    enterCodeToResetPass();
                    //todo saat kontrolü ekle
                } else {
                    Toast.makeText(ForgotPassEnterCodePopActivity.this, getApplicationContext().getString(R.string.passMatch), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void enterCodeToResetPass() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        String resultCode;
        try {
            URL url = new URL(Utility.resetPassChangePassURL);
            connection = new HttpConnection();

            JSONObject json = new JSONObject();
            json.put("email", userMail);
            json.put("code", enterCodeToResetPass.getText().toString());
            json.put("newpass", enterNewPass.getText().toString());

            connection.connectToServerPost(url, json);

            //Get Response
            InputStream is = connection.httpURLConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line).append('\r');
            }
            rd.close();

            json = new JSONObject(response.toString());
            resultCode = json.getString("response");

            if (json.get("response").equals("Password Sucessfully Changed!")) {
                Toast.makeText(ForgotPassEnterCodePopActivity.this, getApplicationContext().getString(R.string.passChange), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, MainPageActivity.class);
                startActivity(i);
                connection.disconnectFromServer();
            } else if (json.get("response").equals(Utility.weakPass)) {
                Toast.makeText(ForgotPassEnterCodePopActivity.this,
                        getApplicationContext().getString(R.string.passControl),
                        Toast.LENGTH_SHORT).show();
            } else if (json.get("response").equals(Utility.codeNotMatch)) {
                Toast.makeText(ForgotPassEnterCodePopActivity.this, getApplicationContext().getString(R.string.codeMatch), Toast.LENGTH_SHORT).show();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setPopWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.ALPHA_CHANGED);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int height = dm.heightPixels;
        int width = dm.widthPixels;

        getWindow().setLayout((int) (width * 0.8), (int) (height * 0.8));
    }
}
