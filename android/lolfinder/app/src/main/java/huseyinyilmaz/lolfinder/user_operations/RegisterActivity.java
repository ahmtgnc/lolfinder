package huseyinyilmaz.lolfinder.user_operations;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.constant.Region;
import net.rithms.riot.dto.Summoner.Summoner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.rest.HttpConnection;
import huseyinyilmaz.lolfinder.util.Utility;

public class RegisterActivity extends AppCompatActivity {

    TextView serverUsernameView;
    EditText username;
    EditText mail;
    EditText password;
    EditText passwordAgain;
    Button registerButton;
    Spinner servers;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Spinner color
        Spinner spinner = (Spinner) findViewById(R.id.registerServerSpinner);
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.lolorange), PorterDuff.Mode.SRC_ATOP);


        username = (EditText) findViewById(R.id.registerUsernameEdit);
        mail = (EditText) findViewById(R.id.registerMailEdit);
        password = (EditText) findViewById(R.id.registerPassEdit);
        passwordAgain = (EditText) findViewById(R.id.registerPassReEdit);
        registerButton = (Button) findViewById(R.id.registerPageBtn);
        servers = (Spinner) findViewById(R.id.registerServerSpinner);

        serverUsernameView = (TextView) findViewById(R.id.registerUsernameLbl);

        final RiotApi api = new RiotApi(Utility.apiKey);

        final List<String> serverList = new ArrayList<>();
        serverList.add(getApplicationContext().getString(R.string.Turkey));
        serverList.add(getApplicationContext().getString(R.string.NorthAmerica));
        serverList.add(getApplicationContext().getString(R.string.EUWEST));
        serverList.add(getApplicationContext().getString(R.string.EUN));
        serverList.add(getApplicationContext().getString(R.string.LatinAmericaNor));
        serverList.add(getApplicationContext().getString(R.string.LatinAmericaSou));
        serverList.add(getApplicationContext().getString(R.string.Brasil));
        serverList.add(getApplicationContext().getString(R.string.Russia));
        serverList.add(getApplicationContext().getString(R.string.Ocean));
        serverList.add(getApplicationContext().getString(R.string.Japan));
        serverList.add(getApplicationContext().getString(R.string.Korea));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item, serverList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servers.setAdapter(dataAdapter);

        servers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                serverUsernameView.setText(servers.getSelectedItem().toString()
                        + " "+getApplicationContext().getString(R.string.userServerControl));
                ((TextView) adapterView.getChildAt(0)).setGravity(Gravity.CENTER);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                servers.setSelection(0);
                serverUsernameView.setText(servers.getSelectedItem().toString()
                        + " "+getApplicationContext().getString(R.string.userServerControl));
            }
        });


        registerButton.setOnClickListener(new View.OnClickListener() {

            HttpConnection connection = new HttpConnection();

            @Override
            public void onClick(View view) {

                String serverName = Utility.getShorterServerName(servers.getSelectedItem().toString()
                        , getApplicationContext());
                Summoner summoner = null;
                ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
                pd.setMessage(getApplicationContext().getString(R.string.username) + serverName + " "+getApplicationContext().getString(R.string.serverControl));
                pd.setCancelable(false);
                pd.show();
                try {
                    summoner = api.getSummonerByName(
                            Enum.valueOf(Region.class, serverName),
                            username.getText().toString());
                } catch (RiotApiException e) {
                    e.printStackTrace();
                } finally {
                    pd.cancel();
                    pd.dismiss();
                }

                if (username.getText().length() < 3) {
                    Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.usernameChar), Toast.LENGTH_SHORT).show();
                } else if (mail.getText().length() < 5) {
                    Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.mailChar), Toast.LENGTH_SHORT).show();
                } else if (password.getText().length() < 4) {
                    Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.passChar), Toast.LENGTH_SHORT).show();
                } else if (!password.getText().toString().equals(passwordAgain.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.passMatch), Toast.LENGTH_SHORT).show();
                }//else if (password.getText())

                if (summoner != null) {
                    register(connection, serverName, summoner);
                }else{
                    Toast.makeText(RegisterActivity.this, servers.getSelectedItem().toString()
                            + " "+getApplicationContext().getString(R.string.serverUser), Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

    private void register(HttpConnection connection, String serverName, Summoner summoner) {

        JSONObject json = new JSONObject();
        ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
        pd.setCancelable(false);
        pd.setMessage(getApplicationContext().getString(R.string.registering));
        pd.show();
        try {
            json.put("username", username.getText().toString());
            json.put("email", mail.getText().toString());
            json.put("password", password.getText().toString());
            json.put("server", servers.getSelectedItem().toString());

            URL url = new URL(Utility.registerURL);

            JSONObject jsonObject = Utility.getResponse(connection.connectToServerPost(url, json));
            Log.e("json objesi", jsonObject.toString());
            Log.e(username.getText().toString(),summoner.toString());
            if (jsonObject.get("response").toString().equals(Utility.usernameAlreadyReg)) {
                Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.usernameControl), Toast.LENGTH_SHORT).show();
            } else if (jsonObject.get("response").toString().equals(Utility.weakPass)) {
                Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.passControl), Toast.LENGTH_SHORT).show();
            } else if (jsonObject.get("response").toString().equals(Utility.emailNotValid)) {
                Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.mailControl), Toast.LENGTH_SHORT).show();
            } else if (jsonObject.get("response").toString().equals(Utility.registerSuc)) {
                Toast.makeText(RegisterActivity.this, getApplicationContext().getString(R.string.registerSuc), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(RegisterActivity.this, MainPageActivity.class);
                startActivity(i);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pd.cancel();
            pd.dismiss();
        }

    }
}
