package huseyinyilmaz.lolfinder.user_operations;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.rest.HttpConnection;
import huseyinyilmaz.lolfinder.util.Utility;

public class ChangePassPopActivity extends AppCompatActivity {

    EditText oldPass;
    EditText newPass;
    EditText newPassRepeat;
    Button changePass;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_pop);
        setPopWindow();
        sharedPreferences = getSharedPreferences(MainPageActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);


        oldPass = (EditText) findViewById(R.id.changePassOldPassEdt);
        newPass = (EditText) findViewById(R.id.changePassNewPassEdt);
        newPassRepeat = (EditText) findViewById(R.id.changePassNewPassReEdt);
        changePass = (Button) findViewById(R.id.changePasschangeBtn);

        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    changePass();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String changePass() throws IOException {
        String resultCode = "";
        HttpConnection connection = null;
        try {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

            URL url = new URL(Utility.changePassURL);
            connection = new HttpConnection();
            String username = sharedPreferences.getString("username", "");

            JSONObject json = new JSONObject();
            json.put("username", username.toLowerCase());
            json.put("oldpass", oldPass.getText().toString());
            json.put("newpass", newPass.getText().toString());
            if (newPass.getText().toString().equals(newPassRepeat.getText().toString())) {
                resultCode = connection.connectToServerPost(url, json).getResponseMessage();
                //Get Response
                InputStream is = connection.httpURLConnection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line).append('\r');
                }
                rd.close();

                json = new JSONObject(response.toString());
                resultCode = json.getString("response");

                if (json.get("response").equals(Utility.changePassSuc)) {
                    Toast.makeText(ChangePassPopActivity.this, getApplicationContext().getString(R.string.passChange), Toast.LENGTH_SHORT).show();
                    logoutAfterPassChange();
                    Intent i = new Intent(this, MainPageActivity.class);
                    startActivity(i);
                    connection.disconnectFromServer();
                } else if (json.get("response").equals(Utility.weakPass)) {
                    Toast.makeText(ChangePassPopActivity.this, getApplicationContext().getString(R.string.passControl), Toast.LENGTH_SHORT).show();
                } else if (json.get("response").equals(Utility.passNotMatch)) {
                    Toast.makeText(ChangePassPopActivity.this, getApplicationContext().getString(R.string.passError), Toast.LENGTH_SHORT).show();
                } else if (json.get("response").equals(Utility.errorChangePass)) {
                    Toast.makeText(ChangePassPopActivity.this, getApplicationContext().getString(R.string.serverError), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getApplicationContext().getString(R.string.passMatch), Toast.LENGTH_SHORT).show();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //if (connection != null)
            //    connection.disconnectFromServer();
        }
        return resultCode;
    }

    private void setPopWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.ALPHA_CHANGED);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int height = dm.heightPixels;
        int width = dm.widthPixels;

        getWindow().setLayout((int) (width * 0.8), (int) (height * 0.8));
    }

    private void logoutAfterPassChange() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
