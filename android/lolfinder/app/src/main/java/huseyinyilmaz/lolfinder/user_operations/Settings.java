package huseyinyilmaz.lolfinder.user_operations;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import huseyinyilmaz.lolfinder.R;

/**
 * Created by huseyinyilmaz on 10.07.2017.
 */

public class Settings extends AppCompatActivity {
    Button changePassBtn2;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        changePassBtn2 = (Button)findViewById(R.id.changePassBtn2);
        changePassBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Settings.this, ChangePassPopActivity.class);
                startActivity(i);
            }
        });




}
}
