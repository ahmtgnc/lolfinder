package huseyinyilmaz.lolfinder.after_login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.user_operations.MainPageActivity;
import huseyinyilmaz.lolfinder.util.Utility;

public class UserMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    SharedPreferences sharedpreferences;
    String profileNameFromURL;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
//*********************
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//*********************
        sharedpreferences = getSharedPreferences(MainPageActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);
        String username = sharedpreferences.getString("username", "");
        final TextView welcomeMessage = (TextView) findViewById(R.id.welcome);
        final TextView welcomeLbl = (TextView) findViewById(R.id.welcomeLbl);

        TextView statusLbl = (TextView) findViewById(R.id.statusLbl);
        welcomeMessage.setText(username);
        //server.setText(sharedpreferences.getString("server",""));
        //summonerName.setText(username);

        welcomeMessage.postDelayed(new Runnable() {
            public void run() {
                welcomeMessage.setVisibility(View.INVISIBLE);
                welcomeLbl.setVisibility(View.INVISIBLE);

            }
        }, 2000);


        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            statusLbl.setText(" ("+getApplicationContext().getString(R.string.connectionOnline)+")");
            statusLbl.setTextColor(getResources().getColor(R.color.green));

        }
        else {
            statusLbl.setText(" ("+getApplicationContext().getString(R.string.connectionOffline)+")");
            statusLbl.setTextColor(getResources().getColor(R.color.red));
        }



    }
    //***************************************
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finishAffinity();
            System.exit(0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_main, menu);
        ImageView summonerProfImage = (ImageView) findViewById(R.id.appBarImageView);
        TextView server = (TextView) findViewById(R.id.appBarServer);
        TextView summonerName = (TextView) findViewById(R.id.appBarSummonerName);
        TextView summonerLevel = (TextView) findViewById(R.id.appBarSummonerLevel);
        summonerProfImage.setImageDrawable(Utility.getCircularImage(
                getProfileImageFromURL(sharedpreferences.getInt("profileImageId", 1))));
        server.setText(sharedpreferences.getString("server",""));
        summonerName.setText(sharedpreferences.getString("username",""));
        summonerLevel.setText(String.valueOf(sharedpreferences.getLong("summonerLevel", 30)) + " " +
                getApplicationContext().getText(R.string.SummonerLevel));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.profile) {
            //todo profile sayfasına gidilecek.

            Intent i = new Intent(UserMain.this, UserProfile.class);
            startActivity(i);

        } else if (id == R.id.counters) {
            //todo counter sayfası eklenecek.
        } else if (id == R.id.find) {
            //todo my game sayfası eklenecek.
        } else if (id == R.id.logoutMenu) {
            SharedPreferences sharedpreferences = getSharedPreferences(MainPageActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.apply();
            Intent i = new Intent(UserMain.this, MainPageActivity.class);
            startActivity(i);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Bitmap getProfileImageFromURL(int profImageId){
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        Bitmap bitmap = null;
        try {
            InputStream in = new URL("http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/" + profImageId + ".png").openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
//******************************
}
