package huseyinyilmaz.lolfinder.user_operations;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import huseyinyilmaz.lolfinder.R;

public class UserOperationsActivity extends AppCompatActivity {

    Button changePassBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        changePassBtn = (Button) findViewById(R.id.changePassBtn);

        changePassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserOperationsActivity.this, ChangePassPopActivity.class);
                startActivity(i);
            }
        });

    }
}
