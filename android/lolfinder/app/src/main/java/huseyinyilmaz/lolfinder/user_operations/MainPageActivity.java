package huseyinyilmaz.lolfinder.user_operations;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.constant.Region;
import net.rithms.riot.dto.Summoner.Summoner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.after_login.UserMain;
import huseyinyilmaz.lolfinder.rest.HttpConnection;
import huseyinyilmaz.lolfinder.util.Utility;

public class MainPageActivity extends AppCompatActivity {

    Button loginBtn;
    EditText tvUsername;
    EditText tvPassword;
    TextView register;
    TextView forgotPass;
    TextView languageLbl;
    ImageView img_trflag;
    ImageView img_ukflag;

    SharedPreferences sharedpreferences;
    ProgressDialog pd;
    Configuration config;

    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        pd = new ProgressDialog(MainPageActivity.this);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        tvUsername = (EditText) findViewById(R.id.usernameIn);
        tvPassword = (EditText) findViewById(R.id.passIn);
        register = (TextView) findViewById(R.id.registerLbl);
        languageLbl = (TextView) findViewById(R.id.languageLbl);
        forgotPass = (TextView) findViewById(R.id.forgotPasswordLbl);
        img_trflag = (ImageView) findViewById(R.id.img_trflag);
        img_ukflag = (ImageView) findViewById(R.id.img_ukflag);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
        }
        else
            Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.noConnection), Toast.LENGTH_LONG).show();

        final SharedPreferences sharedpreferences = getSharedPreferences(MainPageActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);
        String username = sharedpreferences.getString("username", "");
        String language = sharedpreferences.getString("language", "");

        if (username != null && !username.equals("")) {
            Intent i = new Intent(MainPageActivity.this, UserMain.class);
            startActivity(i);
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                if (tvUsername.getText().length() < 3) {
                    Toast.makeText(MainPageActivity.this, getApplicationContext().getString(R.string.usernameChar), Toast.LENGTH_SHORT).show();
                } else if (tvPassword.getText().length() < 5) {
                    Toast.makeText(MainPageActivity.this, getApplicationContext().getString(R.string.passChar), Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        pd.setMessage(getApplicationContext().getString(R.string.install));
                        pd.setCancelable(false);
                        pd.show();
                        Log.i("login answer ", tryToLogin());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                }
                else
                Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.noConnection), Toast.LENGTH_LONG).show();
            }
        });
        tvUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    tvUsername.setHint("");
                else{
                    tvUsername.setHint(getApplicationContext().getString(R.string.usernameIn));}
            }
        });

        tvPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    tvPassword.setHint("");
                else{
                    tvPassword.setHint(getApplicationContext().getString(R.string.passIn));}
            }
        });


        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainPageActivity.this, ForgotPasswordPopActivity.class);
                startActivity(i);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainPageActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });


        if (!language.equals("")) {
            img_trflag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeLang("");
                    recreate();
                }
            });
        }
        if (!language.equals("en")) {
            img_ukflag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeLang("en");
                    recreate();
                }
            });
        }
    }

    private String tryToLogin() throws IOException {
        String resultCode = "";
        HttpConnection connection = null;
        RiotApi api = new RiotApi(Utility.apiKey);
        Summoner summoner = new Summoner();

        try {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

            URL url = new URL(Utility.loginURL);
            connection = new HttpConnection();
            String username = tvUsername.getText().toString();
            String password = tvPassword.getText().toString();


            JSONObject json = new JSONObject();
            json.put("username", username.toLowerCase());
            json.put("password", password);
            resultCode = connection.connectToServerPost(url, json).getResponseMessage();

            //Get Response
            InputStream is = connection.httpURLConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line).append('\r');
            }
            rd.close();

            json = new JSONObject(response.toString());
            resultCode = json.getString("response");
            String server = json.getString("server");
            summoner = api.getSummonerByName(Utility.getServerNameToRegion(server, getApplicationContext()), username);
            if (json.get("response").equals(Utility.loginSuc)) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("username", summoner.getName());
                editor.putString("server", server);
                editor.putInt("profileImageId", summoner.getProfileIconId());
                editor.putLong("summonerLevel", summoner.getSummonerLevel());
                editor.commit();
                Intent i = new Intent(this, UserMain.class);
                startActivity(i);
            } else if (json.get("response").equals(Utility.invalidPass)) {
                Toast.makeText(MainPageActivity.this, getApplicationContext().getString(R.string.loginError), Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (json.get("response").equals(Utility.userNotExist)) {
                Toast.makeText(MainPageActivity.this, getApplicationContext().getString(R.string.userFindError), Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RiotApiException e) {
            e.printStackTrace();
        } finally {
            connection.disconnectFromServer();
        }
        return resultCode;

    }

    public void changeLang(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("language");
        editor.putString("language", lang);
        editor.commit();
    }
    public void onBackPressed() {
            finishAffinity();
            System.exit(0);


    }

}

