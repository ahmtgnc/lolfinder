package huseyinyilmaz.lolfinder.user_operations;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import huseyinyilmaz.lolfinder.R;
import huseyinyilmaz.lolfinder.rest.HttpConnection;
import huseyinyilmaz.lolfinder.util.Utility;

public class ForgotPasswordPopActivity extends AppCompatActivity {

    EditText mailToResetPass;
    Button resetPass;
    HttpConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setPopWindow();

        mailToResetPass = (EditText) findViewById(R.id.forgotPassMailEdit);
        resetPass = (Button) findViewById(R.id.forgotPassResetPassBtn);

        resetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPass();
            }
        });

    }

    private void resetPass() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        String resultCode;
        try {
            URL url = new URL(Utility.resetPassURL);
            connection = new HttpConnection();
            JSONObject json = new JSONObject();
            json.put("email", mailToResetPass.getText().toString());

            connection.connectToServerPost(url, json);

            //Get Response
            InputStream is = connection.httpURLConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line).append('\r');
            }
            rd.close();

            json = new JSONObject(response.toString());
            resultCode = json.getString("response");

            if (json.get("response").equals(Utility.checkMailCode)) {
                //Toast.makeText(ForgotPasswordPopActivity.this, "Şifre Başarıyla Değiştirildi", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, ForgotPassEnterCodePopActivity.class);
                i.putExtra("mail", mailToResetPass.getText().toString());
                startActivity(i);
                connection.disconnectFromServer();
            } else if (json.get("response").equals(Utility.errorResetPass)) {
                Toast.makeText(ForgotPasswordPopActivity.this,
                        getApplicationContext().getString(R.string.serverError),
                        Toast.LENGTH_SHORT).show();
            } else if (json.get("response").equals(Utility.mailNotExists)) {
                Toast.makeText(ForgotPasswordPopActivity.this, getApplicationContext().getString(R.string.mailFind), Toast.LENGTH_SHORT).show();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setPopWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.ALPHA_CHANGED);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int height = dm.heightPixels;
        int width = dm.widthPixels;

        getWindow().setLayout((int) (width * 0.8), (int) (height * 0.8));
    }
}
