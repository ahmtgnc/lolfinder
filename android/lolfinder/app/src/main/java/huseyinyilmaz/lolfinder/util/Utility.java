package huseyinyilmaz.lolfinder.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;

import net.rithms.riot.api.RiotApi;
import net.rithms.riot.constant.Region;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import huseyinyilmaz.lolfinder.R;

/**
 * Created by ahmetgenc on 26/06/2017.
 */

public class Utility {

    RiotApi riotApi;

    /**
     * URLs localhosts

     public static String registerURL = "http://10.0.2.2:11111/register";
     public static String loginURL = "http://10.0.2.2:11111/login";
     public static String changePassURL = "http://10.0.2.2:11111/api/change_pass";
     public static String resetPassURL = "http://10.0.2.2:11111/api/resetpass";
     public static String resetPassChangePassURL = "http://10.0.2.2:11111/api/resetpass/chg";
     */
    /**
     * URLs heroku
     */
    public static String registerURL = "http://lolfinder-back.herokuapp.com/register";
    public static String loginURL = "http://lolfinder-back.herokuapp.com/login";
    public static String changePassURL = "http://lolfinder-back.herokuapp.com/api/change_pass";
    public static String resetPassURL = "http://lolfinder-back.herokuapp.com/api/resetpass";
    public static String resetPassChangePassURL = "http://lolfinder-back.herokuapp.com/api/resetpass/chg";

    /**
     * Result messages
     */
    public static String changePassSuc = "Password Sucessfully Changed";
    public static String weakPass = "New Password is Weak. Try a Strong Password!";
    public static String passNotMatch = "Passwords do not match. Try Again!";
    public static String errorChangePass = "Error while changing password!";
    public static String codeNotMatch = "Code does not match. Try Again!";
    public static String mailNotExists = "Email Does not Exists.";
    public static String errorResetPass = "Error While Resetting password. Try Again!";
    public static String checkMailCode = "Check your Email and enter the verification code to reset your Password.";
    public static String invalidPass = "Invalid Password";
    public static String userNotExist = "User not exist";
    public static String loginSuc = "Login Success";
    public static String usernameAlreadyReg = "Username already Registered";
    public static String emailNotValid = "Email Not Valid";
    public static String registerSuc = "Sucessfully Registered";

    /**
     * riot api key
     */
    public static String apiKey = "RGAPI-08fc2493-7328-4304-888a-70ba5ef68397";

    public static JSONObject getResponse(HttpURLConnection connection) throws IOException, JSONException {

        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            response.append(line).append('\r');
        }
        rd.close();

        JSONObject json = new JSONObject(response.toString());

        return json;

    }

    public static String getShorterServerName(String servers, Context context) {
        String shortenServerName = "";

        Map<String, String> nameAndShorten = new HashMap<>();

        nameAndShorten.put(context.getString(R.string.NorthAmerica), "NA");
        nameAndShorten.put(context.getString(R.string.EUWEST), "EUW");
        nameAndShorten.put(context.getString(R.string.EUN), "EUNE");
        nameAndShorten.put(context.getString(R.string.LatinAmericaNor), "LAN");
        nameAndShorten.put(context.getString(R.string.LatinAmericaSou), "LAS");
        nameAndShorten.put(context.getString(R.string.Brasil), "BR");
        nameAndShorten.put(context.getString(R.string.Turkey), "TR");
        nameAndShorten.put(context.getString(R.string.Russia), "RU");
        nameAndShorten.put(context.getString(R.string.Ocean), "OCE");
        nameAndShorten.put(context.getString(R.string.Japan), "JP");
        nameAndShorten.put(context.getString(R.string.Korea), "KR");

        return nameAndShorten.get(servers);
    }

    public static Region getServerNameToRegion(String server, Context context){
        Region region;
        String shortServerName = "";
        Map<String, String> nameAndShorten = new HashMap<>();

        nameAndShorten.put(context.getString(R.string.NorthAmerica), "NA");
        nameAndShorten.put(context.getString(R.string.EUWEST), "EUW");
        nameAndShorten.put(context.getString(R.string.EUN), "EUNE");
        nameAndShorten.put(context.getString(R.string.LatinAmericaNor), "LAN");
        nameAndShorten.put(context.getString(R.string.LatinAmericaSou), "LAS");
        nameAndShorten.put(context.getString(R.string.Brasil), "BR");
        nameAndShorten.put(context.getString(R.string.Turkey), "TR");
        nameAndShorten.put(context.getString(R.string.Russia), "RU");
        nameAndShorten.put(context.getString(R.string.Ocean), "OCE");
        nameAndShorten.put(context.getString(R.string.Japan), "JP");
        nameAndShorten.put(context.getString(R.string.Korea), "KR");

        for (Map.Entry<String, String> entries : nameAndShorten.entrySet()) {
            if(entries.getKey().equals(server)){
                shortServerName = entries.getValue();
            }
        }

        region = Region.valueOf(shortServerName);

         return region;
    }

    public static Drawable getCircularImage(Bitmap bitmap){
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(Resources.getSystem(),
                bitmap);
        roundDrawable.setCircular(true);
        return roundDrawable;
    }

    public RiotApi getRiotApi() {
        riotApi = new RiotApi(apiKey);
        return riotApi;
    }
}
