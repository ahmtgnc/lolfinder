var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = mongoose.Schema({
    token: String
    , username: String
    , email: String
    , hashed_password: String
    , server: String
    , salt: String
    , temp_str: String
});
mongoose.connect('mongodb://admin:admin@ds139942.mlab.com:39942/lolfinder');
//temporal mongoDB free service
module.exports = mongoose.model('users', userSchema);
