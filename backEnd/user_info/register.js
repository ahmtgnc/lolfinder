var crypto = require('crypto');
var rand = require('csprng');
var mongoose = require('mongoose');
var user = require('../model/models');

exports.register = function (username, email, password, server, callback) {
    var x = username.toLowerCase();
    var y = email;
    var z = server;
    if (!(0 == y.length)) {
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && password.length > 4 && password.match(/[0-9]/)) {
            var temp = rand(160, 36);
            var newpass = temp + password;
            console.log(email + " kullanıcın girdiği email");
            var token = crypto.createHash('sha512').update(username.toLocaleLowerCase + rand).digest("hex");
            var hashed_password = crypto.createHash('sha512').update(newpass).digest("hex");
            var newuser = new user({
                token: token
                , username: x
                , email: email
                , hashed_password: hashed_password
                , server: server
                , salt: temp
            });
            user.find({
                email: email
            }, function (err, users) {
                var len = users.length;
                if (len == 0) {
                    newuser.save(function (err) {
                        callback({
                            'response': "Sucessfully Registered"
                        });
                    });
                }
                else {
                    callback({
                        'response': "Email already Registered"
                    });
                }
            });
//            user.find({
//                username: username
//            }, function (err, users) {
//                var len = users.length;
//                if (len == 0) {
//                    newuser.save(function (err) {
//                        callback({
//                            'response': "Sucessfully Registered"
//                        });
//                    });
//                }
//                else {
//                    callback({
//                        'response': "Username already Registered"
//                    });
//                }
//            });
        }
        else {
            callback({
                'response': "New Password is Weak. Try a Strong Password!"
            });
        }
    }
    else {
        callback({
            'response': "Email Not Valid"
        });
    }
}
