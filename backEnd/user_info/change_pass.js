var crypto = require('crypto');
var rand = require('csprng');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var user = require('../model/models');
var smtpTransport = nodemailer.createTransport({
    service: 'Gmail'
    , auth: {
        user: "lolfinderapp@gmail.com"
        , pass: "Asd123123"
    }
});
exports.cpass = function (username, opass, npass, callback) {
    var temp1 = rand(160, 36);
    var newpass1 = temp1 + npass;
    var hashed_passwordn = crypto.createHash('sha512').update(newpass1).digest("hex");
    console.log(hashed_passwordn + " hashed_passwordn");
    user.find({
        username: username
    }, function (err, users) {
        if (users.length != 0) {
            var temp = users[0].salt;
            var hash_db = users[0].hashed_password;
            var newpass = temp + opass;
            var hashed_password = crypto.createHash('sha512').update(newpass).digest("hex");
            if (hash_db == hashed_password) {
                if (npass.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && npass.length > 4 && npass.match(/[0-9]/)) {
                    user.findOne({
                        username: username
                    }, function (err, doc) {
                        doc.hashed_password = hashed_passwordn;
                        doc.salt = temp1;
                        doc.save();
                        callback({
                            'response': "Password Sucessfully Changed"
                            , 'res': true
                        });
                    });
                }
                else {
                    callback({
                        'response': "New Password is Weak. Try a Strong Password!"
                        , 'res': false
                    });
                }
            }
            else {
                callback({
                    'response': "Passwords do not match. Try Again!"
                    , 'res': false
                });
            }
        }
        else {
            callback({
                'response': "Error while changing password!"
                , 'res': false
            });
        }
    });
}

