var chgpass = require('./user_info/change_pass');
var rstpass = require('./user_info/forgot_pass');
var register = require('./user_info/register');
var login = require('./user_info/login');
module.exports = function (app) {
    app.get('/', function (req, res) {
        res.end("LolFinder-Node-Android-Project");
    });
    app.post('/login', function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        login.login(username, password, function (found) {
            console.log(found);
            res.json(found);
        });
    });
    app.post('/register', function (req, res) {
        var username = req.body.username;
        var email = req.body.email;
        var password = req.body.password;
        var server = req.body.server;
        register.register(username, email, password, server, function (found) {
            console.log(found);
            res.json(found);
        });
    });
    app.post('/api/change_pass', function (req, res) {
        var username = req.body.username;
        var opass = req.body.oldpass;
        var npass = req.body.newpass;
        chgpass.cpass(username, opass, npass, function (found) {
            console.log(found);
            res.json(found);
        });
    });
    app.post('/api/resetpass', function (req, res) {
        var email = req.body.email;
        rstpass.respass_init(email, function (found) {
            console.log(found);
            res.json(found);
        });
    });
    app.post('/api/resetpass/chg', function (req, res) {
        var email = req.body.email;
        var code = req.body.code;
        var newPassword = req.body.newpass;
        rstpass.respass_chg(email, code, newPassword, function (found) {
            console.log(found);
            res.json(found);
        });
    });
};
